from src.TradeEngine import TradeEngine

import pytest
import os

test_csv = os.listdir(r"../Samples")


@pytest.mark.parametrize('path', test_csv)
def test_read_from_csv(path):
    engine = TradeEngine()
    engine.read_from_csv("../Samples/" + path)
    assert engine.order_book != {}, "Order Book should not be empty!"


@pytest.mark.parametrize('path', test_csv)
def test_order_is_removed_from_book_when_remainingQTY_isZero(path):
    engine = TradeEngine()
    engine.read_from_csv("../Samples/" + path)
    for orderbook in engine.order_book.values():
        for orders in orderbook.asks_dict.values():
            assert [order.remaining_quantity > 0 for order in orders], "Order is not removed in asks_dict when remaining_quantity <=0"
        for orders in orderbook.bids_dict.values():
            assert [order.remaining_quantity > 0 for order in orders], "Order is not removed in bids_dict when remaining_quantity <=0"


@pytest.mark.parametrize('path', test_csv)
def test_order_is_removed_from_book_when_remainingQTY_isZero(path):
    engine = TradeEngine()
    engine.read_from_csv("../Samples/" + path)
    for orderbook in engine.order_book.values():
        for ask in orderbook.asks_dict.keys():
            assert len(orderbook.asks_dict[ask]) > 0, "Ask price is not removed when underlying orders are 0"
        for bid in orderbook.bids_dict.keys():
            assert len(orderbook.bids_dict[bid]) > 0, "Ask price is not removed when underlying orders are 0"


def test_order_can_partial_fill(capsys):
    engine = TradeEngine()
    engine.add_order_to_book("Order1", "0700.HK", "200", "Sell", 20000)
    engine.add_order_to_book("Order4", "0700.HK", "200", "Buy", 10000)
    captured = capsys.readouterr()
    assert captured.out == """Ack,Order1,0700.HK,200,Sell,20000
Ack,Order4,0700.HK,200,Buy,10000
Fill,Order1,0700.HK,200.0,Sell,20000,200.0,10000
Fill,Order4,0700.HK,200.0,Buy,10000,200.0,10000
""", "Order cannot be partially filled!!"


def test_use_lower_price_if_buyPrice_is_higher(capsys):
    engine = TradeEngine()
    engine.add_order_to_book("Order1", "0700.HK", "200", "Sell", 20000)
    engine.add_order_to_book("Order4", "0700.HK", "300", "Buy", 20000)
    captured = capsys.readouterr()
    assert captured.out == """Ack,Order1,0700.HK,200,Sell,20000
Ack,Order4,0700.HK,300,Buy,20000
Fill,Order1,0700.HK,200.0,Sell,20000,200.0,20000
Fill,Order4,0700.HK,300.0,Buy,20000,200.0,20000
""", "Deal Price is not matched when incoming buy price is higher that book sell price!"

def test_use_higher_price_if_sellPrice_is_lower(capsys):
    engine = TradeEngine()
    engine.add_order_to_book("Order1", "0700.HK", "300", "Buy", 20000)
    engine.add_order_to_book("Order4", "0700.HK", "200", "Sell", 20000)
    captured = capsys.readouterr()
    assert captured.out == """Ack,Order1,0700.HK,300,Buy,20000
Ack,Order4,0700.HK,200,Sell,20000
Fill,Order1,0700.HK,300.0,Buy,20000,300.0,20000
Fill,Order4,0700.HK,200.0,Sell,20000,300.0,20000
""", "Deal Price is not matched when incoming sell price is lower that book buy price!"

def test_limit_order_matching(capsys):
    engine = TradeEngine()
    engine.read_from_csv("../Samples/orders1.csv")
    captured = capsys.readouterr()
    assert captured.out == """Ack,Order1,0700.HK,610,Sell,20000
Ack,Order2,0700.HK,610,Sell,10000
Ack,Order3,0700.HK,610,Buy,10000
Fill,Order1,0700.HK,610.0,Sell,20000,610.0,10000
Fill,Order3,0700.HK,610.0,Buy,10000,610.0,10000
""", "LimitOrder Failed!"


def test_mkt_order_priority(capsys):
    engine = TradeEngine()
    engine.read_from_csv("../Samples/orders2.csv")
    captured = capsys.readouterr()
    assert captured.out == """Ack,Order1,0700.HK,610,Sell,20000
Ack,Order2,0700.HK,MKT,Sell,10000
Ack,Order3,0700.HK,610,Buy,10000
Fill,Order2,0700.HK,MKT,Sell,10000,610.0,10000
Fill,Order3,0700.HK,610.0,Buy,10000,610.0,10000
""", "MarketOrder is no prioritized!"


def test_order_will_be_rejected_if_qty_is_large(capsys):
    engine = TradeEngine()
    engine.add_order_to_book("Order1", "0700.HK", "MKT", "Sell", 10000000)
    captured = capsys.readouterr()
    assert captured.out == """Reject,Order1,0700.HK,MKT,Sell,10000000
""", "Order with qty > 1000000 has been added to book!"


def test_order_will_be_rejected_if_qty_is_negative(capsys):
    engine = TradeEngine()
    engine.add_order_to_book("Order1", "0700.HK", "MKT", "Sell", -1000)
    engine.add_order_to_book("Order4", "0700.HK", "200", "Sell", 0)
    captured = capsys.readouterr()
    assert captured.out == """Reject,Order1,0700.HK,MKT,Sell,-1000
Reject,Order4,0700.HK,200,Sell,0
""", "Negative quantity is accepted!"


def test_order_price_is_float(capsys):
    engine = TradeEngine()
    engine.add_order_to_book("Order1", "0700.HK", "200.1", "Sell", 1000)
    captured = capsys.readouterr()
    assert captured.out == """Ack,Order1,0700.HK,200.1,Sell,1000
""", "MarketOrder did not fill or filled after limit order"


def test_multi_symbol(capsys):
    engine = TradeEngine()
    engine.read_from_csv("../Samples/orders4.csv")
    captured = capsys.readouterr()
    assert captured.out == """Ack,Order1,0700.HK,610.0,Sell,10000
Ack,Order2,0005.HK,49.8,Sell,10000
Ack,Order3,0005.HK,49.8,Buy,10000
Fill,Order2,0005.HK,49.8,Sell,10000,49.8,10000
Fill,Order3,0005.HK,49.8,Buy,10000,49.8,10000
""", "Multiple Symbol is not supported!"


def test_order_will_be_rejected_if_side_is_not_Buy_Sell(capsys):
    engine = TradeEngine()
    engine.add_order_to_book("Order1", "0700.HK", "MKT", "sdfg", 1000)
    captured = capsys.readouterr()
    assert captured.out == """Reject,Order1,0700.HK,MKT,sdfg,1000
""", "MarketOrder did not fill or filled after limit order"


def test_book_has_only_mkt_orders(capsys):
    engine = TradeEngine()
    engine.read_from_csv("../Samples/orders6.csv")
    captured = capsys.readouterr()
    assert captured.out == """Ack,Order11,0700.HK,MKT,Sell,20000
Ack,Order22,0700.HK,MKT,Buy,10000
Ack,Order33,0700.HK,MKT,Buy,10000
Ack,Order44,0700.HK,MKT,Sell,20000
Ack,Order54,0700.HK,MKT,Buy,10000
Ack,Order66,0700.HK,MKT,Buy,10000
Ack,Order1,213.HK,MKT,Buy,10000
""", "MarketOrder has been filled when no limit price!"


def test_MktOdr_will_be_filled_when_LmtOdr_comes(capsys):
    engine = TradeEngine()
    engine.read_from_csv("../Samples/orders5.csv")
    captured = capsys.readouterr()
    assert captured.out == """Ack,Order1,0700.HK,MKT,Sell,20000
Ack,Order2,0700.HK,MKT,Buy,10000
Ack,Order3,0700.HK,610,Buy,10000
Fill,Order1,0700.HK,MKT,Sell,20000,610.0,10000
Fill,Order2,0700.HK,MKT,Buy,10000,610.0,10000
Fill,Order1,0700.HK,MKT,Sell,20000,610.0,10000
Fill,Order3,0700.HK,610.0,Buy,10000,610.0,10000
""", "MarketOrder did not fill or filled after limit order"


def test_MktOdr_shouldnot_be_filled_after_LmtOdr_comes(capsys):
    engine = TradeEngine()
    engine.read_from_csv("../Samples/orders7.csv")
    captured = capsys.readouterr()
    assert captured.out == """Ack,Order1,0700.HK,MKT,Sell,20000
Ack,Order2,0700.HK,MKT,Buy,10000
Ack,Order3,0700.HK,610,Buy,10000
Fill,Order1,0700.HK,MKT,Sell,20000,610.0,10000
Fill,Order2,0700.HK,MKT,Buy,10000,610.0,10000
Fill,Order1,0700.HK,MKT,Sell,20000,610.0,10000
Fill,Order3,0700.HK,610.0,Buy,10000,610.0,10000
Ack,Order4,0700.HK,MKT,Buy,20000
Ack,Order5,0700.HK,MKT,Sell,10000
""", "MarketOrder filled after limit order is executed!"


def test_LimitBuyOrder_shouldnot_fill_if_bid_lower_than_ask(capsys):
    engine = TradeEngine()
    engine.add_order_to_book("Order1", "0700.HK", "100", "Buy", 10000)
    engine.add_order_to_book("Order4", "0700.HK", "200", "Sell", 10000)
    captured = capsys.readouterr()
    assert captured.out == """Ack,Order1,0700.HK,100,Buy,10000
Ack,Order4,0700.HK,200,Sell,10000
""", "Limit Buy order is filled when bid > ask!"


def test_LimitSellOrder_shouldnot_fill_if_bid_lower_than_ask(capsys):
    engine = TradeEngine()
    engine.add_order_to_book("Order1", "0700.HK", "200", "Sell", 10000)
    engine.add_order_to_book("Order4", "0700.HK", "100", "Buy", 10000)
    captured = capsys.readouterr()
    assert captured.out == """Ack,Order1,0700.HK,200,Sell,10000
Ack,Order4,0700.HK,100,Buy,10000
""", "Limit Sell order is filled when bid > ask!"
