# CS AES Trade Engine

Background
1.	Order matching engines are one of the core components in modern exchanges
2.	This component accepts buy and sell orders from clients and matches them up according to various algorithms
3.	One algorithm adopted by most exchanges is price

## Functional Requirements

![img.png](png/requirements.png)


# Data File

The Trade Engine is very easy to use for simple order matching purposes. For now, it supports two types of orders,
Market and Limited Order. Simply get your csv file ready and run main.py to get started 

The engine reads the following format from the csv file.

OrderID, Symbol, Price, Side, OrderQuantity

please see below for example.

OrderID,Symbol,Price,Side,OrderQuantity

Order1,0700.HK,610,Sell,20000

Order2,0700.HK,610,Sell,10000

Order3,0700.HK,610,Buy,10000

Please kindly Note the following:

1. OrderID should be unqiue for each session of trading operations
2. Symbol is a string type that you may put any text 
3. Price should not be exceeding 1,000,000 as 1,000,000 is default price for an market orders
4. Max. OrderQuantity is 1,000,000, exceeding the limit and the order will be rejected


# Getting Started

The program is written in python3.9. please run main.py and you may see the follwoing.

![img_1.png](png/intro.png)

Once you have input the correct filepath, the program will read the data and process the results accordingly.

![img.png](png/order1.png)

you may keep going with other data files or input reset to clear the previous record.

if you have finished the trading, please input exit to close the program.

![img.png](png/full_log.png)

# Coding Logic

![img.png](png/diagram.png)

When the Trade Engine receive a filepath, it will start the following logic.

1. Check if the Order is valid, reject invalid orders. If the order is valid, create an order based on the price.
   1. Market order if the price == "MKT", default price = 1000000 for buy order and -1000000 for sell order
   2. Limited order if the price is a float  
2. Search if there are existing orderbook created for the target symbol. Create an Orderbook if not exist.
3. Locate the Orderbook by target symbol. Add the order to orderbook.
4. If the order is a LimitOrder, check if there are pending market order in orderbook. if there are pending MarketOrders, matches all MarketOrders by the limit price given
5. Locate the best exiting price on the market. Match trades if the price is valid (bid > ask).
6. Updates remaining_quantity in the order and the orderbook. Remove unnecessary orders and bid/ask price from the orderbook
7. Updates the best_price and continue trading until no matched orders are found
8. If the order has not been completed. store it in the orderbook with their price as key.
9. Print executed info and process next order

## Thank you for Reading!
Please let me know if there are any questions/Room to improve. 

Contacts
pokhei421@gmail.com 
66882631
