from src.TradeEngine import TradeEngine


def main():
    """
    main entry function
    :return:
    """
    exit_command = 'quit'
    reset_command = 'reset'
    engine = TradeEngine()  # start engine

    print("Welcome to the Simple Trade Engine made by Jack Chan!\n"
          "Here are 3 commands for the program.\n"
          "1. Input the csv filepath containing your orders\n"
          f"2. Input {reset_command} to remove all previous order records\n"
          f"3. Input {exit_command} to exit the program\n")
    while True:
        data_input = input(f"PLease specifics filepath or command: ")
        if data_input == exit_command:
            print("Thank you for using the Trade Engine!")
            break
        if data_input == reset_command:
            print("Order Book has been reset")
            engine.reset()
            continue
        engine.read_from_csv(data_input)


if __name__ == "__main__":
    main()
