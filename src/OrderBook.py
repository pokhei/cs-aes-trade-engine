from src.Order import *


class OrderBook:
    """
    bid/ask book for single symbol

    TODO: add support for Order Cancelling/Amending
    """

    def __init__(self, symbol):
        """
        book_id: str
        asks_dict: {int, Order}
        bids_dict: {int,Order}

        :param symbol: str, identify book symbol
        """
        self.book_id = symbol
        self.asks_dict = {}
        self.bids_dict = {}

    def find_best_price(self, order, target_dict):
        """
        helper function to find best matching price in the current order book

        :param order: Order, MarketOrder or LimitOrder
        :param target_dict: {int, Order}, reference to asks_dict or bids_dict
        :return: float
        """

        best_price = None   # return None if not found
        # Case MarketOrder, return highest/lowest price for buy/sell order, ignore existing market order
        if order.__type__() == MarketOrder and order.side == Side.Buy and len(target_dict) > 1 and target_dict.get(
                Order.MIN_ORDER_PRICE) is not None:
            return min([k for k in target_dict.keys() if k != Order.MIN_ORDER_PRICE])
        elif order.__type__() == MarketOrder and order.side == Side.Sell and len(target_dict) > 1 and target_dict.get(
                Order.MAX_ORDER_PRICE) is not None:
            return max([k for k in target_dict.keys() if k != Order.MAX_ORDER_PRICE])
        # Case LimitOrder, return highest/lowest price for buy/sell order including market order
        elif order.__type__() == LimitOrder and order.side == Side.Buy and len(target_dict) > 0:
            return min(target_dict.keys())
        elif order.__type__() == LimitOrder and order.side == Side.Sell and len(target_dict) > 0:
            return max(target_dict.keys())

        return best_price

    def add_order(self, incoming_order: Order):
        """
        Add to current OrderBook when receiving an incoming Rrder

        :param incoming_order: Order, MarketOrder or LimitOrder
        :return:
        """

        target_dict = {}  # counterparty book dict, ask if buy, bid if sell
        book_dict = {}  # book dict where the order belongs, bid if buy, ask if sell

        # set params by trading Side
        if incoming_order.side == Side.Buy:
            target_dict = self.asks_dict
            book_dict = self.bids_dict
            mkt_order_price = Order.MIN_ORDER_PRICE
        else:
            target_dict = self.bids_dict
            book_dict = self.asks_dict
            mkt_order_price = Order.MAX_ORDER_PRICE

        best_price = self.find_best_price(incoming_order, target_dict)  # best price on book

        # When receiving a LimitOrder, check if any MarketOrders are pending, match MarketOrders first if so
        if target_dict.get(mkt_order_price) is not None and book_dict.get(
                -mkt_order_price) is not None and incoming_order.__type__() == LimitOrder:
            for order in target_dict.get(mkt_order_price):
                if book_dict.get(-mkt_order_price) is None:
                    break
                book_order = book_dict.get(-mkt_order_price)[0]
                match_qty = min(order.remaining_quantity, book_order.remaining_quantity)
                order.remaining_quantity -= match_qty
                book_order.remaining_quantity -= match_qty
                if order.id > book_order.id:
                    self.executed_info(book_order, order, self.book_id, incoming_order.price, match_qty)
                else:
                    self.executed_info(order, book_order, self.book_id, incoming_order.price, match_qty)
                # updates orderbook after trade
                if book_order.remaining_quantity == 0:
                    del book_dict[-mkt_order_price][0]
                if len(book_dict[-mkt_order_price]) == 0:
                    del book_dict[-mkt_order_price]
                if order.remaining_quantity == 0:
                    del target_dict[mkt_order_price][0]
                if len(target_dict[mkt_order_price]) == 0:
                    del target_dict[mkt_order_price]

        def whileClause():
            """

            :return: True if there are matching trades, False vice versa
            """
            if incoming_order.__type__() == MarketOrder:
                return best_price is not None and incoming_order.remaining_quantity > 0 and best_price != Order.MIN_ORDER_PRICE and best_price != Order.MAX_ORDER_PRICE
            elif incoming_order.side == Side.Buy:
                return best_price is not None and incoming_order.remaining_quantity > 0 and incoming_order.price >= best_price
            else:
                return best_price is not None and incoming_order.remaining_quantity > 0 and incoming_order.price <= best_price

        # Loop until no suitable trades are occurring.
        while whileClause():

            if target_dict.get(Order.MIN_ORDER_PRICE) is None:
                book_order = target_dict[best_price][0]
            elif incoming_order.side == Side.Buy:
                book_order = target_dict[Order.MIN_ORDER_PRICE][0]
            elif incoming_order.side == Side.Sell:
                book_order = target_dict[Order.MAX_ORDER_PRICE][0]

            if book_order.__type__() == MarketOrder and incoming_order.__type__() == MarketOrder:
                # set deal price as best price if both market order
                deal_price = best_price
            elif book_order.__type__() == LimitOrder and incoming_order.__type__() == LimitOrder:
                # choose lower price for buy order if both limit order
                deal_price = min(best_price, incoming_order.price) if incoming_order.side == Side.Buy else max(
                    best_price, incoming_order.price)
            else:
                # if one of them is market order, choose the max price for buy order since bid price is always Order.MIN_ORDER_PRICE
                deal_price = max(best_price, incoming_order.price) if incoming_order.side == Side.Buy else min(
                    best_price, incoming_order.price)

            # execute trade
            match_qty = min(incoming_order.remaining_quantity, book_order.remaining_quantity)
            incoming_order.remaining_quantity -= match_qty
            book_order.remaining_quantity -= match_qty
            self.executed_info(book_order, incoming_order, self.book_id, deal_price, match_qty)
            if book_order.remaining_quantity == 0:
                del target_dict[best_price][0]
            if len(target_dict[best_price]) == 0:
                del target_dict[best_price]

            # updates best price
            best_price = self.find_best_price(incoming_order, target_dict)

        # Add incoming order to book if no matches are found
        if incoming_order.remaining_quantity > 0:
            self.append_to_book(incoming_order, book_dict)

    def executed_info(self, book_order: Order, incoming_order: Order, book_id, best_price, match_qty):
        """
        Produce results for the trade

        TODO: add Trade class for saving historical trades

        :param book_order:
        :param incoming_order:
        :param book_id:
        :param best_price:
        :param match_qty:
        :return:
        """
        print(
            f"Fill,{book_order.order_id},{book_id},{book_order.get_price_str()},{book_order.get_side_str()},{book_order.order_quantity},{best_price},{match_qty}")
        print(
            f"Fill,{incoming_order.order_id},{book_id},{incoming_order.get_price_str()},{incoming_order.get_side_str()},{incoming_order.order_quantity},{best_price},{match_qty}")

    def append_to_book(self, incoming_order: Order, book_dict):
        """
        Add to order book after trades

        :param incoming_order: Order, MarketOrder or LimitOrder
        :param book_dict: {}, bid/ask dict
        :return:
        """
        depth = book_dict.setdefault(incoming_order.price, [])
        depth.append(incoming_order)

