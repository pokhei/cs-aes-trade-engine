from tqdm import tqdm

from src.OrderBook import OrderBook
from src.Order import *

import pandas as pd


class TradeEngine:
    """
        Trade Engine class
    """

    def __init__(self):
        """
        self.order_book: {symbol: OrderBook} saves order book depth by the order Symbol
        """
        self.order_book = {}

    def read_from_csv(self, filepath: str):
        """
        Read orders from csv

        :param filepath: str, relative or absolute path to the order csv file
        :return:
        """
        df_dict = {}

        try:
            df_dict = pd.read_csv(filepath).to_dict('records')
        except(Exception, pd.errors.EmptyDataError) as e:
            print(e.args)


        if df_dict != {}:
            for row in tqdm(df_dict):
                self.add_order_to_book(row["OrderID"], row['Symbol'], row['Price'], row['Side'],
                                       row['OrderQuantity'])

    def reset(self):
        self.order_book = {}

    def add_order_to_book(self, order_id, symbol, price, side, order_quantity):
        """
        Add an order to order book

        :param order_id: str, order id
        :param symbol: str
        :param price: str
        :param side: str
        :param order_quantity: int, max 1000000
        :return:
        """
        if self.assert_order(order_id, symbol, price, side, order_quantity):
            if price == "MKT":
                order = MarketOrder(order_id, side, order_quantity)
            else:
                order = LimitOrder(order_id, price, side, order_quantity)

            book = self.order_book.setdefault(symbol, OrderBook(symbol))
            book.add_order(order)

    @staticmethod
    def assert_order(order_id, symbol, price, side, order_quantity):
        """
        Help to identify errors in the orders

        :param order_id: str
        :param symbol: str
        :param price: str
        :param side: str
        :param order_quantity:  int
        :return:
        """
        if order_quantity > 1000000 or order_quantity <= 0:
            print(f"Reject,{order_id},{symbol},{price},{side},{order_quantity}")
            return False
        elif side != "Sell" and side != "Buy":
            print(f"Reject,{order_id},{symbol},{price},{side},{order_quantity}")
            return False
        else:
            print(f"Ack,{order_id},{symbol},{price},{side},{order_quantity}")
            return True
