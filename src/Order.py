from enum import Enum


class Side(Enum):
    Buy = 0
    Sell = 1


class Order:
    """
    Order super class, save information of an Order

    TODO: add support for CancelOrder, AmendingOrder
    """
    MAX_ORDER_PRICE = 10000000
    MIN_ORDER_PRICE = -10000000

    def __init__(self, order_id: str, side: str, order_quantity: int):
        """

        :param order_id: str
        :param side: str
        :param order_quantity: int
        """
        self.order_id = order_id
        if side == 'Buy':
            self.side = Side.Buy
        else:
            self.side = Side.Sell
        self.order_quantity = self.remaining_quantity = order_quantity
        self.id = int(order_id[5:])

    def __type__(self):
        return self.__class__

    def get_side_str(self):
        return 'Buy' if self.side == Side.Buy else 'Sell'


class MarketOrder(Order):
    """

    """
    def __init__(self, order_id: str, side: str, order_quantity: int):
        super().__init__(order_id, side, order_quantity)
        # set MarketOrder default price as MAX_ORDER_PRICE or MIN_ORDER_PRICE based on the Side
        if self.side == Side.Buy:
            self.price = self.MAX_ORDER_PRICE
        else:
            self.price = self.MIN_ORDER_PRICE

    def get_price_str(self):
        return 'MKT'


class LimitOrder(MarketOrder):
    """
    LimitOrder is a subclass of MarketOrders
    """

    def __init__(self, order_id: str, price: str, side: str, order_quantity: int):
        super().__init__(order_id, side, order_quantity)
        self.price = float(price)

    def get_price_str(self):
        return str(self.price)
